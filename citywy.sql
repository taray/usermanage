/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50732
Source Host           : 127.0.0.1:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2023-08-02 10:50:48
*/

SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
                          `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
                          `title` varchar(40) NOT NULL COMMENT '课程标题',
                          `logo` varchar(200) DEFAULT NULL COMMENT '缩略图',
                          `summary` varchar(400) DEFAULT NULL COMMENT '课程摘要',
                          `content` text COMMENT '课程内容',
                          `tag` varchar(100) DEFAULT NULL COMMENT '标签',
                          `e_menu` int(20) DEFAULT NULL,
                          `e_aid` int(20) NOT NULL COMMENT '编辑者,外键系统用户表',
                          `e_save` datetime DEFAULT NULL COMMENT '创建时间,默认当前系统时间',
                          `e_chg` datetime DEFAULT NULL COMMENT '编辑时间,默认为当前系统时间',
                          `state` int(4) DEFAULT '0' COMMENT '审核状态（未发布0, 发布1）',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='课程表';

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '课程1', 'static.citywy.com/pic/c9b5a742-5107-4bb0-a115-26fa20322559.png', '课程简介内容', '<p style=\"text-indent:2em;\">123</p><p style=\"text-indent:2em;\">我身后是和平，我面前是战争</p><p style=\"text-indent:2em;\">拿起钢枪，就要放下儿女情长</p><p style=\"text-indent:2em;\">穿上军装，就要舍弃舒适安逸</p><figure class=\"image\"><img src=\"https://p3-sign.toutiaoimg.com/tos-cn-i-qvj2lq49k0/ed30cda643b04c15b89a4d62884b986e~noop.image?_iz=58558&amp;from=article.pc_detail&amp;x-expires=1691547972&amp;x-signature=xtmdmZhRDw4E%2FH7mBE0CG8J0jv4%3D\"></figure>', '课程标签', '1', '1', '2023-08-02 10:39:26', '2023-08-02 10:39:26', '1');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
                        `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键,自增长',
                        `name` varchar(10) NOT NULL COMMENT '栏目名称',
                        `code` varchar(20) NOT NULL COMMENT '栏目标识,唯一,构成访问链接一部分',
                        `level` int(1) NOT NULL COMMENT '栏目级别,1=一级栏目,2=二级栏目,3=三级栏目',
                        `scope` varchar(40) NOT NULL COMMENT '使用范围,以,隔开,（资讯：N 知识S...)',
                        `note` varchar(60) DEFAULT NULL COMMENT '备注说明',
                        `pid` int(20) DEFAULT NULL COMMENT '父级主键,外键引用本身',
                        `state` tinyint(1) NOT NULL COMMENT '启用状态,true=启用,flase=禁用,默认true',
                        `sort` int(2) DEFAULT NULL COMMENT '排序号',
                        PRIMARY KEY (`id`),
                        KEY `fk_menu_e_pid` (`pid`),
                        KEY `uu_menu_code_level_scope` (`code`,`level`,`scope`),
                        CONSTRAINT `fk_menu_e_pid` FOREIGN KEY (`pid`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '资讯栏目1', 'aqcs', '1', 'A', null, null, '1', '1');
INSERT INTO `menu` VALUES ('2', '资讯栏目2', 'ZXLM2', '1', 'A', null, null, '1', '2');
INSERT INTO `menu` VALUES ('3', '课程栏目1', 'KCLM1', '1', 'C', null, null, '1', '1');
INSERT INTO `menu` VALUES ('4', '课程栏目2', 'KCLM2', '1', 'C', null, null, '1', '2');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
                        `title` varchar(40) NOT NULL COMMENT '要闻标题',
                        `logo` varchar(200) DEFAULT NULL COMMENT '缩略图',
                        `summary` varchar(400) DEFAULT NULL COMMENT '要闻摘要',
                        `content` text COMMENT '要闻内容',
                        `tag` varchar(100) DEFAULT NULL COMMENT '标签',
                        `e_menu` int(20) DEFAULT NULL COMMENT '所属栏目,外键引用栏目表,业务逻辑层上不为空',
                        `e_aid` int(20) NOT NULL COMMENT '编辑者,外键系统用户表',
                        `e_save` datetime DEFAULT NULL COMMENT '创建时间,默认当前系统时间',
                        `e_chg` datetime DEFAULT NULL COMMENT '编辑时间,默认为当前系统时间',
                        `state` int(4) DEFAULT '0' COMMENT '审核状态（审核中0，已通过1，未通过2，已发布3）',
                        `source` varchar(100) DEFAULT NULL COMMENT '来源',
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='新闻表';

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('2', '资讯测试1', 'static.citywy.com/pic/b7183d33-edd6-4c00-a810-2eada6a2f873.png', '安全知识新闻', '<p style=\"text-indent:2em;\">\r\n	安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻安全知识新闻\r\n</p>', '安全, 知识, 测试', '1', '1', '2023-05-23 11:43:56', '2023-05-23 11:43:56', '1', '来源1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
                        `id` int(4) NOT NULL AUTO_INCREMENT,
                        `name` varchar(255) NOT NULL,
                        `age` int(4) DEFAULT NULL,
                        `address` varchar(255) DEFAULT NULL,
                        `email` varchar(255) DEFAULT NULL,
                        `birthday` date DEFAULT NULL,
                        `password` varchar(80) NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'taray', '12', 'jiangsu', '11232321@112.com', null, '123456');
INSERT INTO `user` VALUES ('2', 'lisi', '29', '上海', '1232132@163.com', null, '123456aa');

