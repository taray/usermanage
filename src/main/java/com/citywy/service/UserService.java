package com.citywy.service;
 
import com.citywy.domain.User;
import com.citywy.util.DBConnUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class UserService {
	/**
	 * 用户登录
	 * @return
	 * @throws SQLException 
	 */
	public boolean UserLogin(User user) throws SQLException{
		boolean b=false;
		String sql="select * from user where name=? and password=?";
		String [] parameters={user.getName(),user.getPassword()};
		List<Map<String, Object>> datas= DBConnUtil.executeQuery(sql, parameters);
		if(datas.size()>0){
			b=true;
		}
		return b;
	}

	/**
	 * 根据name返回User实例
	 * @return
	 */
	public User getUserByName(String name){
		String sql="select * from user where name=?";
		Object [] args={name};
		List<Map<String, Object>> datas = null;
		try {
			datas = DBConnUtil.executeQuery(sql,args);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Map<String,Object> map = datas.get(0);
		User u = new User();
		u.setId(Integer.parseInt(map.get("id").toString()));
		u.setName(map.get("name").toString());
		u.setAge(Integer.parseInt(map.get("age").toString()));
		u.setAddress(map.get("address").toString());
		u.setEmail(map.get("email").toString());
		u.setBirtyDay((Date)map.get("birthday"));
		u.setPassword(map.get("password").toString());
		return u;
	}
}