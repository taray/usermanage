<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String base = request.getScheme()+"://"+request.getServerName()+(request.getServerPort()!=80?":"+request.getServerPort():"")+request.getContextPath()+"/";
	Map<String,String> menus = (Map<String,String>) request.getAttribute("menus");
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<%=base%>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>添加、修改要闻</title>
		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="admin/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="admin/assets/css/font-awesome.min.css" />
		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="admin/assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="admin/assets/css/chosen.css" />
		<link rel="stylesheet" href="admin/assets/css/datepicker.css" />
		<link rel="stylesheet" href="admin/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="admin/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="admin/assets/css/bootstrap-datetimepicker.css" />
		<link rel="stylesheet" href="admin/assets/css/colorpicker.css" />
		<!-- text fonts -->
		<link rel="stylesheet" href="admin/assets/css/ace-fonts.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="admin/assets/css/ace.min.css" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="admin/assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="admin/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="admin/assets/css/ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="admin/assets/css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<script src="admin/assets/js/ace-extra.min.js"></script>
		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<script src="admin/assets/js/html5shiv.min.js"></script>
		<script src="admin/assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<%@include file="../public/top.jsp" %>
		<div class="main-container" id="main-container">
			<jsp:include page="../public/left.jsp" flush="true">
				<jsp:param name="now" value="news_load"/>
			</jsp:include>
			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">首页</a>
						</li>
						<li>
							<a href="#">添加、修改新闻</a>
						</li>
					</ul>
				</div>
				<div class="page-content">
					<div class="page-content-area">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<form action="/servlets/news/modify.htm" method="post" class="form-horizontal">
									<div class="col-sm-4">
										<div class="form-group">
											<label class="col-sm-3 control-label">知识标题:</label>
											<div class="col-sm-9">
											  <input type="text" class="form-control" value="${pages.title }" name="title">
											  <small class="text-warning">必填项，不超过20字符</small>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">文章来源:</label>
											<div class="col-sm-9">
											  <input type="text" class="form-control" value="${pages.source }" name="source">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">缩略图:</label>
											<div class="input-group col-sm-8" style="padding-left:12px;">
												<input type="text" class="form-control" value="${pages.logo }" name="logo" id="d_0pic">
												<span class="input-group-btn">
													<button type="button" class="btn btn-purple btn-sm" onclick="xUploadPic(this,'d_0pic')">
														选择图片
													</button>
												</span>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3 control-label">栏目:</div>
											<div class="input-group col-sm-9" style="padding-left:12px;">
												<select class="form-control" style="width:80%" id="d_menu1">
													<option value="">请选择</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">知识标签:</label>
											<div class="col-sm-9">
												<div class="inline">
													<input type="text" name="tag" id="form-field-tags" value="${pages.tag }" placeholder="输入标签 ..." />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">知识简介:</label>
											<div class="col-sm-9">
												<textarea class="form-control" rows="6" name="summary">${pages.summary }</textarea>
											  	<small class="text-success">不超过80字符</small>
											</div>
										</div>
									</div>
									<!-- 编辑器 -->
									<div class="col-sm-8">
										<div class="form-group" style="margin-left:0px;">
											<textarea style="height:650px;" name="content" id="d_content">${pages.content}</textarea>
										</div>
									</div>
									<div class="col-sm-11 control-label">
										<!-- 修改时传递的ID -->
										<input type="hidden" value="${pages.id}" name="id">
										<!-- 上传图片 -->
										<input type="file" style="display:none;" name="fileToPic" id="fileToPic">
										<!-- 栏目 -->
										<input type="hidden" name="Emenu" id="d_memu">
										<!-- 地址传递 -->
										<input type="hidden" value="" name="locationhref" id="locationhref">
										<div class="clearfix form-group">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													重置
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn btn-info" type="submit" id="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													提交
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				//栏目加载
				window.setTimeout("lmpd(1)",50);
				// window.setTimeout("lmpd(2)",1000);

				//地址传值
				var locationhref = location.href;
				if(locationhref.indexOf('?')!=-1){
					locationhref = locationhref.substring(locationhref.indexOf('?'));
					$("#locationhref").val(locationhref);
				}else{
					$("#locationhref").val("");
				}

				//提交数据前验证
				$("#submit").click(function(){
					var menu=$("#d_menu1").val();
					if(!$("#d_menu1").val()){
						bootbox.alert({
						    message:'请选择栏目！',
						    size:'small'
						});
						$("#d_menu1").focus();
						return false;
					}
					$("#d_memu").val(menu);
					return true;
				});
			});
			//栏目
			var def1="<%=menus.containsKey(1)?menus.get(1):""%>";
			<%--var def2="<%=menus.containsKey(2)?menus.get(2):""%>";--%>

			function lmpd(m){
				if(m==1){
					xllmdh(1,"A","d_menu1","d_menu2","d_menu3","d_menu4",def1);
					def1=null;
				}/*else if(m==2){
					xllmdh(2,"A","d_menu1","d_menu2","d_menu3","d_menu4",def2);
					def2=null;
				}*/
			}
		</script>
		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='admin/assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>
		<!-- <![endif]-->
		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='admin/assets/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='admin/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="admin/assets/js/bootstrap.min.js"></script>
		<!-- page specific plugin scripts -->
		<!--[if lte IE 8]>
		  <script src="admin/assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="admin/assets/js/jquery-ui.custom.min.js"></script>
		<script src="admin/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="admin/assets/js/chosen.jquery.min.js"></script>
		<script src="admin/assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="admin/assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="admin/assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="admin/assets/js/date-time/moment.min.js"></script>
		<script src="admin/assets/js/date-time/daterangepicker.min.js"></script>
		<script src="admin/assets/js/date-time/bootstrap-datetimepicker.min.js"></script>
		<script src="admin/assets/js/bootstrap-colorpicker.min.js"></script>
		<script src="admin/assets/js/jquery.knob.min.js"></script>
		<script src="admin/assets/js/jquery.autosize.min.js"></script>
		<script src="admin/assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="admin/assets/js/jquery.maskedinput.min.js"></script>
		<script src="admin/assets/js/bootstrap-tag.min.js"></script>
		<script src="admin/assets/js/typeahead.jquery.min.js"></script>
		<!-- ace scripts -->
		<script src="admin/assets/js/ace-elements.min.js"></script>
		<script src="admin/assets/js/ace.min.js"></script>
		<!-- CAIKE common js Start-->
		<script src="admin/assets/js/plugin.js"></script>
		<script src="admin/assets/js/my/file-upload.js"></script>
		<script src="admin/assets/js/my/load-menu.js"></script>
		<script type="text/javascript" src="/editor/kindeditor/kindeditor.js"></script>
		<script type="text/javascript">
			//编辑器初始化
			var editor=null;
			KindEditor.ready(function(K){
				editor=K.create("#d_content",{
					urlType:"domain",
					cssPath:"<%=base%>editor/kindeditor/plugins/code/prettify.css",
					uploadJson:ink.home+"admin/ajax/ckeditor.htm",
					fileManagerJson:null,
					allowFileManager:false
				});
			});
		</script>
		<!-- CAIKE common js END-->
	</body>
</html>