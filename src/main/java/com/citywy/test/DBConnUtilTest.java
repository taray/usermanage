package com.citywy.test;

import com.citywy.domain.News;
import com.citywy.util.DBConnUtil;
import com.citywy.util.DBUtil;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 不带连接池的DBConnUtil测试增删改查
 * 项目里也用的它
 */
public class DBConnUtilTest {
    public static void main(String[] args) throws Exception {
//        testInsert();
//        testInsertAll01();
//        testUpdate();
//        testDelete();
//        testQuery1();
    }

    /**
     * 数据库插入操作
     */
    public static void testInsert() throws SQLException {
        Map<String, Object> map =new HashMap<>();
        map.put("title","小白鞋怎么刷才能刷黑？");
        map.put("logo","https://i.niupic.com/images/2023/05/17/b6Lp.jpg");
        map.put("summary","怎么才能将小白鞋子越刷越黑呢");
        map.put("content","用墨水刷");
        map.put("tag","小白鞋,刷鞋");
        map.put("esave",new Date());
        map.put("echg",new Date());
        map.put("state",1);
        try {
            int num = DBConnUtil.insert("know",map);
            System.out.println("输出插入num"+num);//看到这里输出的修改条数1,插入应该返回插入成功的ID这样的需求，后期要加
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 使用isnert方法for循环插入一万条数据！
     * *****耗时长！******
     * @throws SQLException
     */
    public static void testInsertAll01() throws SQLException{
        long start = System.currentTimeMillis();
        for(int i=0;i<1000;i++){
            Map<String, Object> map =new HashMap<>();
            map.put("title","小白鞋怎么刷才能刷黑？");
            map.put("logo","https://i.niupic.com/images/2023/05/17/b6Lp.jpg");
            map.put("summary","怎么才能将小白鞋子越刷越黑呢");
            map.put("content","用墨水刷");
            map.put("tag","小白鞋,刷鞋");
            map.put("esave",new Date());
            map.put("echg",new Date());
            map.put("state",1);
            DBConnUtil.insert("know",map);
        }
        System.out.println("耗时："+(System.currentTimeMillis() - start));//耗时：47448
    }


    /**
     * 测试更新内容
     */
    public static void testUpdate() throws SQLException {
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put("title","小黑鞋怎么刷才变白？");
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("id",2);
        int count = DBConnUtil.update("know",valueMap,whereMap);
    }

    /**
     * 测试删除内容
     */
    public static void testDelete() throws SQLException {
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("id",2);
        whereMap.put("title","小黑鞋怎么刷才变白？");
        int count = DBConnUtil.delete("know",whereMap);
        System.out.println("删除结果："+ count);
    }

    /**
     * 测试查询
     */
    public static void testQuery1(){
        List<Map<String,Object>> datas = null;
        String condition="id>0";//条件
        String order="id";//按什么字段排序
        String sort="asc";//排序方式asc desc
        int pageNow = 2;//当前第几页
        int pageSize = 20;//每页多少条
        String sql="select * from know where "+condition+" order by "+order+" "+sort+" limit "+(pageNow-1)*pageSize+","+pageSize;
        try {
            datas = DBConnUtil.executeQuery(sql,null);
            for (Map<String,Object> map:datas){
                long id = (Long) map.get("id");
                System.out.println("输出查询的ID:"+id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
