package com.citywy.domain;


import java.util.Date;

public class Course {
    private long id;
    private String title;
    private String logo;
    private String summary;//简介
    private String content;
    private String tag;//标签
    private int emenu;
    private int eaid;
    private Date esave;
    private Date echg;
    private int state;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getEmenu() {
        return emenu;
    }

    public void setEmenu(int emenu) {
        this.emenu = emenu;
    }

    public int getEaid() {
        return eaid;
    }

    public void setEaid(int eaid) {
        this.eaid = eaid;
    }

    public Date getEsave() {
        return esave;
    }

    public void setEsave(Date esave) {
        this.esave = esave;
    }

    public Date getEchg() {
        return echg;
    }

    public void setEchg(Date echg) {
        this.echg = echg;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", logo='" + logo + '\'' +
                ", summary='" + summary + '\'' +
                ", content='" + content + '\'' +
                ", tag='" + tag + '\'' +
                ", emenu=" + emenu +
                ", eaid=" + eaid +
                ", esave=" + esave +
                ", echg=" + echg +
                ", state=" + state +
                '}';
    }
}
