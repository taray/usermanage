package com.citywy.service;

import com.citywy.domain.Menu;
import com.citywy.domain.News;
import com.citywy.util.DBConnUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class MenuService {

    /**
     * 查询列表level scope
     * @param level
     * @param scope
     * @return
     */
    public List<Menu> find(int level,String scope) {
        List<Menu> list=new ArrayList<Menu>();
        String sql="select * from menu where scope='"+scope+"' and level="+level+" order by id asc";
        List<Map<String, Object>> datas = null;
        try {
            datas = DBConnUtil.executeQuery(sql, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Menu menu=null;
        try {
            for (Map<String,Object> map:datas){
                menu=new Menu();
                menu.setId(Integer.parseInt(map.get("id").toString()));
                menu.setName(map.get("name").toString());
                menu.setCode(map.get("code").toString());
                menu.setLevel(Integer.parseInt(map.get("level").toString()));
                menu.setScope(map.get("scope").toString());
                menu.setState((Boolean) map.get("state"));
                menu.setSort(Integer.parseInt(map.get("sort").toString()));
                list.add(menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 查询列表level scope pid
     * @param level
     * @param scope
     * @param pid
     * @return
     */
    public List<Menu> find(int level,String scope,int pid) {
        List<Menu> list=new ArrayList<Menu>();
        String sql="select * from menu where scope='"+scope+"' and level="+level+" and pid="+pid+" order by id asc";
        List<Map<String, Object>> datas = null;
        try {
            datas = DBConnUtil.executeQuery(sql, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Menu menu=null;
        try {
            for (Map<String,Object> map:datas){
                menu=new Menu();
                menu.setId(Integer.parseInt(map.get("id").toString()));
                menu.setName(map.get("name").toString());
                menu.setCode(map.get("code").toString());
                menu.setLevel(Integer.parseInt(map.get("level").toString()));
                menu.setScope(map.get("scope").toString());
                menu.setState((Boolean) map.get("state"));
                menu.setSort(Integer.parseInt(map.get("sort").toString()));
                list.add(menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 根据ID获取Menu
     * @param id
     * @return
     */
    public Menu get(int id) {
        Menu menu = new Menu();
        List<Map<String,Object>> maps = null;
        try {
            maps = DBConnUtil.executeQuery("select * from menu where id="+id,null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(maps!=null && maps.size()>0){
            Map map = maps.get(0);
            menu.setId(Integer.parseInt(map.get("id").toString()));
            menu.setName(map.get("name").toString());
            menu.setCode(map.get("code").toString());
            menu.setLevel(Integer.parseInt(map.get("level").toString()));
            menu.setScope(map.get("scope").toString());
            menu.setNote(map.get("note")!=null?(String)map.get("note"):"");
            menu.setPid(map.get("pid")!=null?(int)map.get("pid"):0);
            menu.setState((Boolean)map.get("state"));
            menu.setSort((int)map.get("sort"));
        }
        return menu;
    }

}
