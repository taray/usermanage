# usermanage
1. 基于servlet+jsp+jdbc的后台管理系统,web.xml配置servlet,无注解版本。
2. 本产品适用于初学Servlet,做一个完整的小项目,低于8岁儿童在家长陪同下食用
3. IDE:idea
4. 数据库：Mysql
5. JDK:1.8
6. 配套前台瑜伽项目 [前台servlet+jsp瑜伽网站 ](https://gitee.com/taray/yogawww) 食用；



## 一、项目知识点
1. filter过滤器;
2. ckeditor kindEditor两个编辑器使用(新闻资讯kindEditor 瑜伽课程CkEditor5);
3. servlet mysql jdbc增删改查;
4. 传统commons-fileupload上传;
5. c3p0连接池DBConnectionPool+DBUtil 和 无连接池DBConnUtil(也是在有连接池上改的)


## 二、TIP
1. citywy.sql是mysql表，在数据库中运行,并自行插入一些测试数据,user用户表密码用的明文;
2. 修改dbconn.properties为你的连接信息;


## 三、待完成工作
1. 用户组等权限表添加和实现;
2.

## 四、热烈欢迎项
0. 其实spring等很多都是封装的servlet，servlet是核心基础，大家一定要熟练精通，毕竟软件是做中学的一门语言；
1. 热烈欢迎初学的大家积极fork给本项目提交分支，加上一些自己学习的servlet功能项，让项目在serlvet案例项目中成为更加完善的项目，我会积极合并的；
2. 这里用的JDBC连接池工具类是非常高可用的一个轮子，基本上就是JDBC常用的增删改查经典工具类了；
3. 异步上传ajaxfileupload.js网上也是非常多的版本，这里也是高可用的版本；


## 五、图片展示
![输入图片说明](src/main/webapp/%E5%9B%BE%E7%89%87.png)
![输入图片说明](src/main/webapp/%E5%9B%BE%E7%89%872.png)
![输入图片说明](src/main/webapp/%E5%9B%BE%E7%89%873.png)
![输入图片说明](src/main/webapp/图片4.png)
