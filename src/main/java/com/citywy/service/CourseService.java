package com.citywy.service;

import com.citywy.domain.Course;
import com.citywy.util.DBUtil;

import java.sql.SQLException;
import java.util.*;

/**
 * DBUtil有连接池操作（其实代码一样）
 */
public class CourseService {
    /**
     * 根据id获取课程详情页
     * @param id
     * @return
     */
    public Course get(long id) {
        Course course = new Course();
        List<Map<String,Object>> maps = null;
        try {
            maps = DBUtil.executeQuery("select * from course where id="+id,null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(maps!=null && maps.size()>0){
            Map map = maps.get(0);
            course.setId(Integer.parseInt(map.get("id").toString()));
            course.setTitle(map.get("title").toString());
            course.setLogo(map.get("logo")!=null?map.get("logo").toString():"");
            course.setSummary(map.get("summary").toString());
            course.setContent(map.get("content").toString());
            course.setTag(map.get("tag").toString());
            course.setEmenu(Integer.parseInt(map.get("e_menu").toString()));
            course.setEaid(Integer.parseInt(map.get("e_aid").toString()));
            course.setEsave((Date)map.get("e_save"));
            course.setState((int)map.get("state"));
        }
        return course;
    }


    /**
     * 获取共计多少条数据
     */
    public int getRowCount(String condition){
        //计算一共多少条
        int rowcount=0;
        List<Map<String, Object>> maps = null;
        try {
            if(condition!=null){
                maps = DBUtil.executeQuery("select count(*) from course where "+condition,null);
            }else{
                maps = DBUtil.executeQuery("select count(*) from course", null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            for (Map<String, Object> map:maps){
                rowcount=  Integer.parseInt(map.get("count(*)").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowcount;
    }

    /**
     * 课程分页
     * @param condition where条件
     * @param pageNow  当前第一页
     * @param pageSize 每页都少条
     * @param order 按什么列排序
     * @param sort  排序方式 asc desc
     * @return
     */
    public List<Course> getCourseList(String condition,int pageNow,int pageSize,String order,String sort){
        List<Course> list=new ArrayList<Course>();
        //查询分页语句
        String sql=null;
        if(condition!=null){
            sql="select * from course where "+condition+" order by "+order+" "+sort+" limit "+(pageNow-1)*pageSize+","+pageSize;
        }else{
            sql="select * from course order by "+order+" "+sort+" limit "+(pageNow-1)*pageSize+","+pageSize;
        }
        List<Map<String, Object>> datas = null;
        try {
            datas = DBUtil.executeQuery(sql, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Course course=null;
        try {
            for (Map<String,Object> map:datas){
                course=new Course();
                course.setId(Integer.parseInt(map.get("id").toString()));
                course.setTitle((String) map.get("title"));
                course.setLogo((String) map.get("logo"));
                course.setEmenu((int)map.get("e_menu"));
                course.setEsave((Date) map.get("e_save"));
                list.add(course);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 添加课程
     * @param course
     * @return
     */
    public boolean addCourse(Course course){
        Map<String,Object> maps = new HashMap<String, Object>();
        maps.put("title",course.getTitle());
        maps.put("logo",course.getLogo());
        maps.put("summary",course.getSummary());
        maps.put("content",course.getContent());
        maps.put("tag",course.getTag());
        maps.put("e_menu",course.getEmenu());
        maps.put("e_aid",course.getEaid());
        maps.put("e_save",new Date());
        maps.put("e_chg",new Date());
        maps.put("state",1);
        int num=0;
        try {
            num = DBUtil.insert("course",maps);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (num>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 修改课程
     * @param course
     * @return
     */
    public boolean updCourse(Course course){
        Map<String,Object> valueMap = new HashMap<String, Object>();
        valueMap.put("title",course.getTitle());
        valueMap.put("logo",course.getLogo());
        valueMap.put("summary",course.getSummary());
        valueMap.put("content",course.getContent());
        valueMap.put("tag",course.getTag());
        valueMap.put("e_menu",course.getEmenu());
        valueMap.put("e_aid",course.getEaid());
        valueMap.put("e_save",new Date());
        valueMap.put("e_chg",new Date());
        valueMap.put("state",1);
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("id",course.getId());
        int num=0;
        try {
            num = DBUtil.update("course",valueMap,whereMap);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (num>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 删除单独某一条
     */
    public boolean delCourse(long id){
        boolean b=false;
        if(id>0){
            Map<String, Object> whereMap = new HashMap<>();
            whereMap.put("id",id);
            int affectRowCount = 0;
            try {
                affectRowCount = DBUtil.delete("course", whereMap);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            if(affectRowCount>0){
                b=true;
            }
        }
        return b;
    }
}