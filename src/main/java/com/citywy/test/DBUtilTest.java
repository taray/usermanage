package com.citywy.test;

import com.citywy.util.DBUtil;

import javax.swing.plaf.synth.SynthTextAreaUI;
import java.sql.SQLException;
import java.util.*;

/**
 * 带连接池的DBUtil测试增删改查
 */
public class DBUtilTest {

    public static void main(String[] args) throws SQLException {
//        testInsert();
//        testInsertAll01();
//        testInsertAll02();
//        testUpdate();
//        testDelete();
//        testQuery1();
//        testQuery2();
//        testQuery3();
//        testQuery4();
    }

    /**
     * 数据库插入操作
     */
    public static void testInsert() throws SQLException {
        Map<String, Object> map =new HashMap<>();
        map.put("title","小白鞋怎么刷才能刷黑？");
        map.put("logo","https://i.niupic.com/images/2023/05/17/b6Lp.jpg");
        map.put("summary","怎么才能将小白鞋子越刷越黑呢");
        map.put("content","用墨水刷");
        map.put("tag","小白鞋,刷鞋");
        map.put("esave",new Date());
        map.put("echg",new Date());
        map.put("state",1);
        DBUtil.insert("know",map);
    }

    /**
     * 使用isnert方法for循环插入一万条数据！
     * *****耗时长！******
     * @throws SQLException
     */
    public static void testInsertAll01() throws SQLException{
        long start = System.currentTimeMillis();
        for(int i=0;i<10000;i++){
            Map<String, Object> map =new HashMap<>();
            map.put("title","小白鞋怎么刷才能刷黑？");
            map.put("logo","https://i.niupic.com/images/2023/05/17/b6Lp.jpg");
            map.put("summary","怎么才能将小白鞋子越刷越黑呢");
            map.put("content","用墨水刷");
            map.put("tag","小白鞋,刷鞋");
            map.put("esave",new Date());
            map.put("echg",new Date());
            map.put("state",1);
            DBUtil.insert("know",map);
        }
        System.out.println("耗时："+(System.currentTimeMillis() - start));//耗时：47448
    }

    /**
     * 使用insertAll方法批量插入一万条到数据库
     * ******耗时短，速度快******
     * @throws SQLException
     */
    public static void testInsertAll02() throws SQLException{
        List<Map<String,Object>> mapList = new ArrayList<>();
        for(int i=0;i<10000;i++){
            Map<String, Object> map =new HashMap<>();
            map.put("title","小白鞋怎么刷才能刷黑？");
            map.put("logo","https://i.niupic.com/images/2023/05/17/b6Lp.jpg");
            map.put("summary","怎么才能将小白鞋子越刷越黑呢");
            map.put("content","用墨水刷");
            map.put("tag","小白鞋,刷鞋");
            map.put("esave",new Date());
            map.put("echg",new Date());
            map.put("state",1);
            mapList.add(map);
        }
        long start = System.currentTimeMillis();
        DBUtil.insertAll("know",mapList);
        System.out.println("耗时："+(System.currentTimeMillis() - start));//耗时：5530
    }

    /**
     * 测试更新内容
     */
    public static void testUpdate() throws SQLException {
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put("title","小黑鞋怎么刷才变白？");
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("id",2);
        int count = DBUtil.update("know",valueMap,whereMap);

    }

    /**
     * 测试删除内容
     */
    public static void testDelete() throws SQLException {
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("id",2);
        whereMap.put("title","小黑鞋怎么刷才变白？");
        int count = DBUtil.delete("know",whereMap);
        System.out.println("删除结果："+ count);
    }


    /**
     * 测试查询1
     */
    public static void testQuery1(){
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("title","小白鞋怎么刷才能刷黑？1");
        List<Map<String, Object>> list = null;
        try {
            list = DBUtil.query("know", whereMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(list!=null && list.size()>0){
            for(Map<String,Object> map:list){
                System.out.println(map.get("id"));
            }
        }
    }

    /**
     * 测试查询2
     * 手写sql条件
     */
    public static void testQuery2() {
        String where = "title = ?  AND id > ? ";
        String[] whereArgs = new String[]{"小白鞋怎么刷才能刷黑？1", "5"};
        try {
            List<Map<String, Object>> list = DBUtil.query("know", where, whereArgs);
            System.out.println(list.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询方式3
     */
    public static void testQuery3() {
        try {
            List<Map<String, Object>> list = DBUtil.query("know", false, null, null, null, null, null, null, null);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * SQL注入问题
     */
    public static void testQuery4() {
        String name = "'1' OR '1'='1'";
        String password = "'1' OR '1'='1'";

        String sql = "SELECT * FROM user WHERE name = " + name + " and password = " + password;
        String where = "name = ?  AND password = ? ";
        String[] whereArgs = new String[]{name, password};

        try {
            //1.SQL注入问题
//            DBUtil.query(sql);
            //2.有效防止SQL注入
            DBUtil.query("user", false, null, where, whereArgs, null, null, null, null);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
