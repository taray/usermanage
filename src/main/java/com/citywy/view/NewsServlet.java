package com.citywy.view;
 
import com.citywy.domain.Menu;
import com.citywy.domain.News;
import com.citywy.domain.User;
import com.citywy.service.MenuService;
import com.citywy.service.NewsService;
import com.citywy.util.PageUtil;
import com.citywy.util.PublicUtil;
import com.citywy.util.UserUtil;
import com.sun.jmx.remote.util.OrderClassLoaders;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
/**
 * 新闻增删改查
 */
public class NewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewsServlet() {
        super();
    }
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
 
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		String uri=request.getRequestURI();
		//截取http://admin.citywy.com/servlets/news/load.htm得到type如load
		String type=uri.substring(uri.lastIndexOf("/")+1,uri.lastIndexOf("."));
		//跳转到对应的增删改查
		if(type!=null){
			if("load".equals(type)){
				load(request,response);
			}else if("modify".equals(type)){
				modify(request,response);
			}else if("list".equals(type)){
				list(request,response);
			}else if("drop".equals(type)){
				drop(request,response);
			}
		}

	}

	/**
	 * 加载页面，包括新页面和修改页面
	 */
	public void load(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		long id = PublicUtil.getParam(request,"id",0l);
		NewsService newsService = new NewsService();
		MenuService menuService = new MenuService();
		News news = newsService.get(id);
		Map<Integer,String> menus = new HashMap<Integer,String>();
		//栏目赋值
		if(news.getId()>0){
			Menu m = menuService.get(news.getEmenu());
			menus.put(m.getLevel(),String.valueOf(m.getId()));
		}
		request.setAttribute("pages",news);
		request.setAttribute("menus",menus);
		request.getRequestDispatcher("/admin/news/news_load.jsp").forward(request, response);
	}

	/**
	 * 增加和修改
	 * @param request
	 * @param response
	 */
	public void modify(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		long id = PublicUtil.getParam(request,"id",0l);
		NewsService newsService = new NewsService();
		User user = UserUtil.getLoginUser(request);
		String title=PublicUtil.getParam(request,"title",null);
		String source=PublicUtil.getParam(request,"source",null);
		String logo = PublicUtil.getParam(request,"logo",null);
		int eMenu = PublicUtil.getParam(request,"Emenu",0);
		String tag =PublicUtil.getParam(request,"tag",null);
		String summary = PublicUtil.getParam(request,"summary",null);
		String content = PublicUtil.getParam(request,"content",null);
		News news = new News();
		news.setTitle(title);
		news.setLogo(logo);
		news.setSummary(summary);
		news.setContent(content);
		news.setTag(tag);
		news.setEmenu(eMenu);
		news.setEaid(user.getId());
		news.setSource(source);
		boolean b = false;
		String message="";
		String locationhref = "";
		if(id>0){
			news.setId(id);
			b = newsService.updNews(news);
			if(b){
				message="修改成功";
			}
			//修改返回时返回之前的筛选条件
			locationhref = request.getParameter("locationhref");
			//正则去掉id=xxx&
			if(locationhref.contains("id=")){
				String regex = "";
				if(locationhref.contains("&")){
					regex = "id=[0-9]*&";
				}else{
					regex = "id=[0-9]*";
				}
				Pattern pat = Pattern.compile(regex);
				Matcher matcher = pat.matcher(locationhref);
				while (matcher.find()) {
					String temp = locationhref.substring(matcher.start(),matcher.end());
					locationhref = locationhref.replaceAll(temp, "");
				}
			}
		}else{
			b = newsService.addNews(news);
			if(b){
				message = "添加成功！";
			}
		}
		//为了传递修改成功的参数，我们这里都用转发，其实springmvc中默认也是转发
//		response.sendRedirect("/servlets/news/list.htm");
		request.setAttribute("message",message);
		request.getRequestDispatcher("/servlets/news/list.htm"+locationhref).forward(request, response);
	}

	/**
	 * 查询列表
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void list(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		NewsService newsService=new NewsService();
		//获取参数
		int pageNow=PublicUtil.getParam(request,"x_now",1);
		int pageSize=PublicUtil.getParam(request,"x_size",25);
		String x_order=PublicUtil.getParam(request,"x_order","id");//根据什么字段排序
		String x_sort=PublicUtil.getParam(request,"x_sort","desc");
		String x_name=PublicUtil.getParam(request,"x_name",null);

		//查询条件
		String condition = null;
		if(x_name!=null && !"".equals(x_name)){
			condition = "title like '%"+x_name+"%'";
		}

		int rowcount=newsService.getRowCount(condition);
		int pageCount=(rowcount-1)/pageSize+1;

		List<News> list=newsService.getNewsList(condition,pageNow,pageSize,x_order,x_sort);

		//分页参数
		PageUtil pageUtil = new PageUtil(pageNow,pageSize,pageCount,rowcount,x_order,x_sort,x_name);

		request.setAttribute("list", list);
		request.setAttribute("pageutil",pageUtil);
		request.getRequestDispatcher("/admin/news/news_list.jsp").forward(request, response);
	}

	/**
	 * 删除单个ID内容
	 * @param request
	 * @param response
	 */
	public void drop(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		long id = PublicUtil.getParam(request,"id",0l);
		if(id>0){
			NewsService newsService = new NewsService();
			boolean b = newsService.delNews(id);
			if(b){
				response.sendRedirect("/servlets/news/list.htm");
			}
		}
	}

}