package com.citywy.service;

import com.citywy.domain.News;
import com.citywy.util.DBConnUtil;

import java.sql.SQLException;
import java.util.*;


/**
 * DBConnUtil无连接池操作
 */
public class NewsService {
    /**
     * 根据id获取要闻详情页
     * @param id
     * @return
     */
    public News get(long id) {
        News news = new News();
        List<Map<String,Object>> maps = null;
        try {
            maps = DBConnUtil.executeQuery("select * from news where id="+id,null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(maps!=null && maps.size()>0){
            Map map = maps.get(0);
            news.setId(Integer.parseInt(map.get("id").toString()));
            news.setTitle(map.get("title").toString());
            news.setLogo(map.get("logo")!=null?map.get("logo").toString():"");
            news.setSummary(map.get("summary").toString());
            news.setContent(map.get("content").toString());
            news.setTag(map.get("tag").toString());
            news.setEmenu(Integer.parseInt(map.get("e_menu").toString()));
            news.setEaid(Integer.parseInt(map.get("e_aid").toString()));
            news.setEsave((Date)map.get("e_save"));
            news.setState((int)map.get("state"));
            news.setSource(map.get("source").toString());
        }
        return news;
    }


    /**
     * 获取共计多少条数据
     */
    public int getRowCount(String condition){
        //计算一共多少条
        int rowcount=0;
        List<Map<String, Object>> maps = null;
        try {
            if(condition!=null){
                maps = DBConnUtil.executeQuery("select count(*) from news where "+condition,null);
            }else{
                maps = DBConnUtil.executeQuery("select count(*) from news", null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            for (Map<String, Object> map:maps){
                rowcount=  Integer.parseInt(map.get("count(*)").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowcount;
    }

    /**
     * 要闻分页
     * @param condition where条件
     * @param pageNow  当前第一页
     * @param pageSize 每页都少条
     * @param order 按什么列排序
     * @param sort  排序方式 asc desc
     * @return
     */
    public List<News> getNewsList(String condition,int pageNow,int pageSize,String order,String sort){
        List<News> list=new ArrayList<News>();
        //查询分页语句
        String sql=null;
        if(condition!=null){
            sql="select * from news where "+condition+" order by "+order+" "+sort+" limit "+(pageNow-1)*pageSize+","+pageSize;
        }else{
            sql="select * from news order by "+order+" "+sort+" limit "+(pageNow-1)*pageSize+","+pageSize;
        }
        List<Map<String, Object>> datas = null;
        try {
            datas = DBConnUtil.executeQuery(sql, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        News news=null;
        try {
            for (Map<String,Object> map:datas){
                news=new News();
                news.setId(Integer.parseInt(map.get("id").toString()));
                news.setTitle((String) map.get("title"));
                news.setLogo((String) map.get("logo"));
                news.setEmenu((int)map.get("e_menu"));
                news.setEsave((Date) map.get("e_save"));
                news.setSource(map.get("source").toString());
                list.add(news);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 添加新闻
     * @param news
     * @return
     */
    public boolean addNews(News news){
        Map<String,Object> maps = new HashMap<String, Object>();
        maps.put("title",news.getTitle());
        maps.put("logo",news.getLogo());
        maps.put("summary",news.getSummary());
        maps.put("content",news.getContent());
        maps.put("tag",news.getTag());
        maps.put("e_menu",news.getEmenu());
        maps.put("e_aid",news.getEaid());
        maps.put("e_save",new Date());
        maps.put("e_chg",new Date());
        maps.put("state",1);
        maps.put("source",news.getSource());
        int num=0;
        try {
            num = DBConnUtil.insert("news",maps);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (num>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 修改新闻
     * @param news
     * @return
     */
    public boolean updNews(News news){
        Map<String,Object> valueMap = new HashMap<String, Object>();
        valueMap.put("title",news.getTitle());
        valueMap.put("logo",news.getLogo());
        valueMap.put("summary",news.getSummary());
        valueMap.put("content",news.getContent());
        valueMap.put("tag",news.getTag());
        valueMap.put("e_menu",news.getEmenu());
        valueMap.put("e_aid",news.getEaid());
        valueMap.put("e_save",new Date());
        valueMap.put("e_chg",new Date());
        valueMap.put("state",1);
        valueMap.put("source",news.getSource());
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("id",news.getId());
        int num=0;
        try {
            num = DBConnUtil.update("news",valueMap,whereMap);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (num>0){
            return true;
        }else{
            return false;
        }
    }

    //删除单独某一条
    public boolean delNews(long id){
        boolean b=false;
        if(id>0){
            Map<String, Object> whereMap = new HashMap<>();
            whereMap.put("id",id);
            int affectRowCount = 0;
            try {
                affectRowCount = DBConnUtil.delete("news", whereMap);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            if(affectRowCount>0){
                b=true;
            }
        }
        return b;
    }
}